---

  - name: 5.1.1 Ensure cron daemon is enabled (Scored)
    systemd:
      name: crond
      enabled: yes
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.1

  - name: 5.1.2 Ensure permissions on /etc/crontab are configured (Scored)
    file:
      path: /etc/crontab
      owner: root
      group: root
      mode: '0600'
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.2

  - name: |
          5.1.3 Ensure permissions on /etc/cron.hourly are configured (Scored)
          5.1.4 Ensure permissions on /etc/cron.daily are configured (Scored)
          5.1.5 Ensure permissions on /etc/cron.weekly are configured (Scored)
          5.1.6 Ensure permissions on /etc/cron.monthly are configured (Scored)
          5.1.7 Ensure permissions on /etc/cron.d are configured (Scored)
    file:
      path: '{{ item }}'
      state: directory
      owner: root
      group: root
      mode: '0700'
    loop:
      - /etc/cron.hourly
      - /etc/cron.daily
      - /etc/cron.weekly
      - /etc/cron.monthly
      - /etc/cron.d
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.3
      - section5.1.4
      - section5.1.5
      - section5.1.6
      - section5.1.7

  - name: 5.1.8 Ensure at/cron is restricted to authorized users (Scored)
    copy:
      dest: '{{ item }}'
      owner: root
      group: root
      mode: '0600'
      content: 'root'
    loop:
      - /etc/cron.allow
      - /etc/at.allow
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.8

  - name: 5.1.8 Ensure at/cron is restricted to authorized users (Scored)
    file:
      path: '{{ item }}'
      state: absent
    loop:
      - /etc/cron.deny
      - /etc/at.deny
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.8

  - name: 5.2.1 Ensure permissions on /etc/ssh/sshd_config are configured (Scored)
          5.2.5 Ensure SSH LogLevel is set to INFO (Scored)
          5.2.6 Ensure SSH X11 forwarding is disabled (Scored)
          5.2.7 Ensure SSH MaxAuthTries is set to 4 or less (Scored)
          5.2.8 Ensure SSH IgnoreRhosts is enabled (Scored)
          5.2.9 Ensure SSH HostbasedAuthentication is disabled (Scored)
          5.2.10 Ensure SSH root login is disabled (Scored)
          5.2.11 Ensure SSH PermitEmptyPasswords is disabled (Scored)
          5.2.12 Ensure SSH PermitUserEnvironment is disabled (Scored)
          5.2.13 Ensure SSH Idle Timeout Interval is configured (Scored)
          5.2.14 Ensure SSH LoginGraceTime is set to one minute or less (Scored)
          5.2.15 Ensure SSH warning banner is configured (Scored)
          5.2.16 Ensure SSH PAM is enabled
          5.2.17 Ensure SSH AllowTcpForwarding is disabled (Scored)
          5.2.18 Ensure SSH MaxStartups is configured (Scored)
          5.2.19 Ensure SSH MaxSessions is set to 4 or less (Scored)
          5.2 Ensure SSH Protocol is set to 2 (Scored)
          5.2 Ensure only approved MAC algorithms are used (Scored)
    template:
      src: templates/etc/ssh/sshd_config.j2
      dest: /etc/ssh/sshd_config
      owner: root
      group: root
      mode: '0600'
      backup: yes
    notify: restart sshd
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.2.1
      - section5.2.5
      - section5.2.6
      - section5.2.7
      - section5.2.8
      - section5.2.9
      - section5.2.10
      - section5.2.11
      - section5.2.12
      - section5.2.13
      - section5.2.14
      - section5.2.15
      - section5.2.16
      - section5.2.17
      - section5.2.18
      - section5.2

  - name: 5.2.2 Ensure SSH access is limited (Scored)
    debug:
      msg: "Ensure SSH access is limited by setting up AllowUsers, AllowGroups, DenyUsers, and DenyGroups in /etc/ssh/sshd_config."
    tags:
      - section05-1
      - section05
      - level1
      - section5.2.2

  - name: 5.2.3 Ensure permissions on SSH private host key files are configured (Scored)
    file:
      path: '/etc/ssh/{{ item }}'
      owner: root
      group: root
      mode: '0600'
    with_fileglob:
      - 'ssh_host_*_key'
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.2.3

  - name: 5.2.4 Ensure permissions on SSH public host key files are configured (Scored)
    file:
      path: '/etc/ssh/{{ item }}'
      owner: root
      group: root
      mode: '0644'
    with_fileglob:
      - 'ssh_host_*_key.pub'
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.2.4

  - name: 5.2.20 Ensure system-wide crypto policy is not over-ridden (Scored)
    lineinfile:
      path: /etc/sysconfig/sshd
      regexp: '(.*)CRYPTO_POLICY(.*)'
      line: '#CRYPTO_POLICY'
      backup: yes
    notify: restart sshd
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.2.20

  - name: 5.3.1 Create custom authselect profile (Scored)
    shell: 'authselect create-profile {{ authselect_profile }} -b sssd --symlink-meta'
    when:
      - use_winbind == 'no'
    tags:
      - section05-1
      - section05
      - level1
      - section5.3.1

  - name: 5.3.1 Create custom authselect profile (Scored)
    shell: 'authselect create-profile {{ authselect_profile }} -b winbind --symlink-meta'
    when:
      - use_winbind == 'yes'
    tags:
      - section05-1
      - section05
      - level1
      - section5.3.1

  - name: |
          5.3.2 Select authselect profile (Scored)
          5.3.3 Ensure authselect includes with-faillock
    shell: 'authselect select custom/{{ authselect_profile }} with-sudo with-faillock without-nullok --force'
    tags:
      - section05-1
      - section05
      - level1
      - section5.3.2
      - section5.3.3

  - name: 5.4.1 Ensure password creation requirements are configured (Scored)
    template:
      src: templates/etc/security/pwquality.conf.j2
      dest: /etc/security/pwquality.conf
      owner: root
      group: root
      mode: '0644'
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.1

  - name: |
          5.4.1 Ensure password creation requirements are configured (Scored)
          5.4.3 Ensure password reuse is limited (Scored)
    lineinfile:
      path: '{{ item }}'
      regexp: '^password(.*)pam_pwquality.so'
      line: "password\trequisite\tpam_pwquality.so try_first_pass local_users_only enforce-for-root retry={{ pwquality_retry }} remember={{ pwhistory_remember }}"
      insertbefore: '^password(.*)pam_unix.so'
      state: present
      backrefs: yes
      backup: yes
    loop:
      - '/etc/authselect/custom/{{ authselect_profile }}/system-auth'
      - '/etc/authselect/custom/{{ authselect_profile }}/password-auth'
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.1
      - section5.4.3

  - name: 5.4.2 Ensure lockout for failed password attempts is configured (Scored)
    lineinfile:
      path: '{{ item.file }}'
      regexp: '{{ item.regexp }}'
      line: "{{ item.line }}"
      backup: yes
      backrefs: yes
    with_items:
      - { file: '/etc/authselect/custom/{{ authselect_profile }}/system-auth', regexp: '^auth(.*)pam_faillock.so preauth(.*)', line: "auth\trequired\tpam_faillock.so preauth silent deny=5 unlock_time=900" }
      - { file: '/etc/authselect/custom/{{ authselect_profile }}/system-auth', regexp: '^auth(.*)pam_faillock.so authfail(.*)', line: "auth\trequired\tpam_faillock.so authfail silent deny=5 unlock_time=900" }
      - { file: '/etc/authselect/custom/{{ authselect_profile }}/password-auth', regexp: '^auth(.*)pam_faillock.so preauth(.*)', line: "auth\trequired\tpam_faillock.so preauth silent deny=5 unlock_time=900" }
      - { file: '/etc/authselect/custom/{{ authselect_profile }}/password-auth', regexp: '^auth(.*)pam_faillock.so authfail(.*)', line: "auth\trequired\tpam_faillock.so authfail silent deny=5 unlock_time=900" }
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.2

  - name: |
          5.4.3 Ensure password reuse is limited (Scored)
          5.4.4 Ensure password hashing algorithm is SHA-512 (Scored)
    lineinfile:
      path: '{{ item }}'
      regexp: '^password(.*)pam_unix.so'
      line: "password\tsufficient\tpam_unix.so sha512 shadow try_first_pass use_authtok remember={{ pwhistory_remember }}"
      state: present
      backrefs: yes
      backup: yes
    loop:
      - '/etc/authselect/custom/{{ authselect_profile }}/system-auth'
      - '/etc/authselect/custom/{{ authselect_profile }}/password-auth'
    notify: update authselect
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.3
      - section5.4.4

  - name: |
          5.5.1.1 Ensure password expiration is 365 days or less (Scored)
          5.5.1.2 Ensure minimum days between password changes is 7 or more (Scored)
          5.5.1.3 Ensure password expiration warning days is 7 or more (Scored)
    lineinfile:
      path: /etc/login.defs
      regexp: '{{ item.regexp }}'
      line: '{{ item.line }}'
      state: present
    loop:
      - { regexp: '^(PASS_MAX_DAYS|#PASS_MAX_DAYS)', line: 'PASS_MAX_DAYS {{ pass_max_days }}' }
      - { regexp: '^(PASS_MIN_DAYS|#PASS_MIN_DAYS)', line: 'PASS_MIN_DAYS {{ pass_min_days }}' }
      - { regexp: '^(PASS_MIN_LEN|#PASS_MIN_LEN)', line: 'PASS_MIN_LEN {{ pass_min_len }}' }
      - { regexp: '^(PASS_WARN_AGE|#PASS_WARN_AGE)', line: 'PASS_WARN_AGE {{ pass_warn_age }}' }
    tags:
      - section05-1
      - section05
      - level1
      - section5.5.1.1
      - section5.5.1.2
      - section5.5.1.3

  - name: 5.5.1.4 Ensure inactive password lock is 30 days or less (Scored)
    command: 'useradd -D -f {{ inactive_account_days }}'
    args:
      warn: no
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.1.4

  - name: 5.5.1.5 Ensure all users last password change date is in the past (Scored)
    debug:
      msg: "Ensure all users last password change date is in the past."
    tags:
      - section05-1
      - section05
      - level1
      - section5.5.1.5

  - name: 5.5.2 Ensure system accounts are secured (Scored)
    shell: awk -F':' '($1!="root" && $1!="sync" && $1!="shutdown" &&$1!="halt" && $3<1000 && $7!="/sbin/nologin" && $7!="/bin/false") {print $1}' /etc/passwd
    failed_when: False
    changed_when: False
    check_mode: no
    register: systemaccounts
    tags:
      - section05-1
      - section05
      - level1
      - section5.5.2

  - name: 5.5.2 Ensure system accounts are secured (Scored)
    user:
      name: '{{ item }}'
      shell: /sbin/nologin
    loop: '{{ systemaccounts.stdout_lines }}'
    when:
      - systemaccounts.stdout_lines is defined
      - systemaccounts.stdout_lines != ""
      - item != "postgres"
    tags:
      - section05-1
      - section05
      - level1
      - section5.5.2

  - name: 5.5.3 Ensure default user shell timeout is 900 seconds or less (Scored)
    debug:
      msg: "TMOUT interferes with long running Ansible tasks and sshd timeouts.  TMOUT provides no real benefit as a user can override the setting even when it is set to a read only value."
    tags:
      - section05-2
      - section05
      - level2
      - section5.5.3

  - name: 5.5.4 Ensure default group for the root account is GID 0 (Scored)
    user:
      name: root
      group: root
    tags:
      - section05-1
      - section05
      - level1
      - section5.5.4

  - name: 5.5.5 Ensure default user umask is 027 or more restrictive (Scored)
    lineinfile:
      path: /etc/login.defs
      regexp: '^(UMASK|#UMASK)'
      line: "UMASK\t\t{{ user_umask }}"
      state: present
      backup: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.5.5

  - name: 5.5.5 Ensure default user umask is 027 or more restrictive (Scored)
    replace:
      path: '{{ item }}'
      regexp: 'umask \b[0-9]{3}\b'
      replace: 'umask {{ user_umask }}'
      backup: yes
    loop:
      - /etc/bashrc
      - /etc/profile
    tags:
      - section05-1
      - section05
      - level1
      - section5.5.5

  - name: 5.5.5 Ensure default user umask is 027 or more restrictive (Scored)
    template:
      src: templates/etc/profile.d/zz_cis.sh.j2
      dest: /etc/profile.d/zz_cis.sh
      owner: root
      group: root
      mode: '0644'
    tags:
      - section05-1
      - section05
      - level1
      - section5.5.5

  - name: 5.6 Ensure root login is restricted to system console (Not Scored)
    copy:
      dest: /etc/securetty
      owner: root
      group: root
      mode: '0644'
      content: ''
      backup: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.6

  - name: 5.7 Ensure access to the su command is restricted (Scored)
    lineinfile:
      path: /etc/pam.d/su
      regexp: '^(#auth|auth)(.*)required(.*)pam_wheel.so(.*)'
      line: 'auth\2required\3pam_wheel.so\4'
      state: present
      backrefs: yes
      backup: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.7