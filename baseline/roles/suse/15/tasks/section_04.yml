---

  - name: 4.1.1.1 Ensure auditd is installed (Scored)
    yum:
      name: audit
      state: present
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.1.1

  - name: 4.1.1.2 Ensure auditd service is enabled (Scored)
    systemd:
      name: auditd
      enabled: yes
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.1.2

  - name: 4.1.1.3 Ensure auditing for processes that start prior to auditd is enabled (Scored)
    command: grep -q "audit=1" /etc/default/grub
    register: grubaudit
    failed_when: False
    changed_when: False
    check_mode: no
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.1.3

  - name: 4.1.1.3 Ensure auditing for processes that start prior to auditd is enabled (Scored)
    replace:
      dest: '/etc/default/grub'
      regexp: '^GRUB_CMDLINE_LINUX="(.*)audit=[0-9]+(.*)"'
      replace: 'GRUB_CMDLINE_LINUX="\1audit=1\2"'
      backup: yes
    when: grubaudit.rc == 0
    notify: update grub
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.1.3

  - name: 4.1.1.3 Ensure auditing for processes that start prior to auditd is enabled (Scored)
    lineinfile:
      path: /etc/default/grub
      regexp: '^GRUB_CMDLINE_LINUX="(.*)"'
      line: 'GRUB_CMDLINE_LINUX="\1 audit=1"'
      backup: yes
      backrefs: yes
    when: grubaudit.rc == 1
    notify: update grub
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.1.3

  - name: 4.1.1.3 Ensure auditing for processes that start prior to auditd is enabled (Scored)
    command: '/sbin/grubby --update-kernel=ALL --args="audit=1"'
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.1.3

  - name: 4.1 Configure System Accounting (auditd)
    copy:
      src: 'files/etc/audit/rules.d/audit.rules'
      dest: /etc/audit/rules.d/audit.rules
      owner: root
      group: root
      mode: '0640'
    notify: build auditd rules
    tags:
      - section04-02
      - section04
      - level2
      - section4.1

  - name: |
          4.1.2.1 Ensure audit log storage size is configured (Not Scored)
          4.1.2.2 Ensure audit logs are not automatically deleted (Scored)
          4.1.2.3 Ensure system is disabled when audit logs are full (Scored)
    lineinfile:
      path: /etc/audit/auditd.conf
      regexp: '{{ item.regexp }}'
      line: '{{ item.line }}'
      state: present
    loop:
      - { regexp: '^(max_log_file\s|#max_log_file\s)', line: 'max_log_file = {{ auditd_max_log_file }}' }
      - { regexp: '^(space_left_action|#space_left_action)', line: 'space_left_action = email' }
      - { regexp: '^(action_mail_acct|#action_mail_acct)', line: 'action_mail_acct = root' }
      - { regexp: '^(admin_space_left_action|#admin_space_left_action)', line: 'admin_space_left_action = {{ auditd_admin_space_left_action }}' }
      - { regexp: '^(max_log_file_action|#max_log_file_action)', line: 'max_log_file_action = rotate' }
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.2.1
      - section4.1.2.2
      - section4.1.2.3

  - name: |
          4.1.3 Ensure events that modify date and time information are collected (Scored)
          4.1.4 Ensure events that modify user/group information are collected (Scored)
          4.1.5 Ensure events that modify the system's network environment are collected (Scored)
          4.1.6 Ensure events that modify the system's Mandatory Access Controls are collected (Scored)
          4.1.7 Ensure login and logout events are collected (Scored)
          4.1.8 Ensure session initiation information is collected (Scored)
          4.1.9 Ensure discretionary access control permission modification events are collected (Scored)
          4.1.10 Ensure unsuccessful unauthorized file access attempts are collected (Scored)
          4.1.12 Ensure successful file system mounts are collected (Scored)
          4.1.13 Ensure file deletion events by users are collected (Scored)
          4.1.14 Ensure changes to system administration scope (sudoers) is collected (Scored)
          4.1.15 Ensure system administrator actions (sudolog) are collected (Scored)
          4.1.16 Ensure kernel module loading and unloading is collected (Scored)
    copy:
      src: 'files/etc/audit/rules.d/cis_{{ ansible_architecture }}.rules'
      dest: /etc/audit/rules.d/cis.rules
      owner: root
      group: root
      mode: '0640'
    notify: build auditd rules
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.3
      - section4.1.4
      - section4.1.5
      - section4.1.6
      - section4.1.7
      - section4.1.8
      - section4.1.9
      - section4.1.10
      - section4.1.12
      - section4.1.13
      - section4.1.14
      - section4.1.15
      - section4.1.16

  - name: 4.1.11 Ensure use of privileged commands is collected (Scored)
    shell: find / -xdev \( -perm -4000 -o -perm -2000 \) -type f | awk '{print "-a always,exit -F path=" $1 " -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged" }'
    failed_when: False
    changed_when: False
    check_mode: no
    register: privcmds
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.11

  - name: 4.1.11 Ensure use of privileged commands is collected (Scored)
    copy:
      dest: /etc/audit/rules.d/privcmds_cis.rules
      owner: root
      group: root
      mode: '0640'
      content: |
        {% for privcmd in privcmds.stdout_lines %}
        {{ privcmd }}
        {% endfor %}
    notify: build auditd rules
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.11

  - name: 4.1.17 Ensure the audit configuration is immutable (Scored)
    copy:
      src: files/etc/audit/rules.d/zz_immutable.rules
      dest: /etc/audit/rules.d/zz_immutable.rules
      owner: root
      group: root
      mode: '0640'
    notify: build auditd rules
    tags:
      - section04-02
      - section04
      - level2
      - section4.1.17

  - name: 4.2.1.1 Ensure rsyslog or syslog-ng is installed (Scored)
    yum:
      name: rsyslog
      state: present
    tags:
      - section04-01
      - section04
      - level1
      - section4.2.1.1

  - name: 4.2.1.2 Ensure rsyslog Service is enabled (Scored)
    systemd:
      name: rsyslog
      enabled: yes
    tags:
      - section04-01
      - section04
      - level1
      - section4.2.1.2

  - name: 4.2.1.3 Ensure rsyslog default file permissions configured (Scored)
    lineinfile:
      path: /etc/rsyslog.conf
      regexp: '^\$FileCreateMode'
      line: '$FileCreateMode 0640'
      insertbefore: BOF
      state: present
    tags:
      - section04-01
      - section04
      - level1
      - section4.2.1.3

  - name: 4.2.1.4 Ensure logging is configured (Not Scored)
    copy:
      src: files/etc/rsyslog.d/cis.conf
      dest: /etc/rsyslog.d/cis.conf
      owner: root
      group: root
      mode: '0644'
    tags:
      - section04-01
      - section04
      - level1
      - section4.2.1.4

  - name: |
          4.2.1.4 Ensure logging is configured (Scored)
          4.2.1.5 Ensure remote rsyslog messages are only accepted on designated log hosts. (Not Scored)
    debug:
      msg: "Ensure splunkforwarder is installed to send logs to remote splunk server."
    tags:
      - section04-01
      - section04
      - level1
      - section4.2.1.4
      - section4.2.1.5

  - name: 4.2.3 Ensure permissions on all logfiles are configured (Scored)
    debug:
      msg: "Ensure permissions on all logfiles are correct."
    tags:
      - section04-01
      - section04
      - level1
      - section4.2.4

  - name: 4.2.4 Ensure logrotate is configured (Not Scored)
    copy:
      src: files/etc/logrotate.d/cis
      dest: /etc/logrotate.d/cis
      owner: root
      group: root
      mode: '0644'
    tags:
      - section04-01
      - section04
      - level1
      - section4.2.4