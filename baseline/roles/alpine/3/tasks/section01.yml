---

  - name: |
          1.1.1.1 Ensure mounting of cramfs filesystem is disabled (Scored)
          1.1.1.2 Ensure mounting of freevxfs filesystem is disabled (Scored)
          1.1.1.3 Ensure mounting of jffs2 filesystem is disabled (Scored)
          1.1.1.4 Ensure mounting of hfs filesystems is disabled (Scored)
          1.1.1.5 Ensure mounting of hfsplus filesystems is disabled (Scored)
          1.1.1.6 Ensure mounting of squashfs filesystem is disabled (Scored)
          1.1.1.7 Ensure mounting of udf filesystem is disabled (Scored)
          1.1.1.8 Ensure mounting of FAT filesystems is disabled (Scored)
    copy:
      src: files/etc/modprobe.d/cis_fs.conf
      dest: /etc/modprobe.d/cis_fs.conf
      owner: root
      group: root
      mode: '0644'
    tags:
      - section01-1
      - section01-2
      - section01
      - level1
      - level2
      - section1.1.1
      - section1.1.1.1
      - section1.1.1.2
      - section1.1.1.3
      - section1.1.1.4
      - section1.1.1.5
      - section1.1.1.6
      - section1.1.1.7
      - section1.1.1.8

  - name: |
          1.1.2 Ensure /tmp is configured (Scored)
          1.1.3 Ensure nodev option set on /tmp parition (Scored)
          1.1.4 Ensure nosuid option set on /tmp parition (Scored)
          1.1.5 Ensure noexec option set on /tmp partition (Scored)
          1.1.6 Ensure separate partition exists for /var (Scored)
          1.1.7 Ensure separate partition exists for /var/tmp (Scored)
          1.1.8 Ensure nodev option set on /var/tmp partition (Scored)
          1.1.9 Ensure nosuid option set on /var/tmp partition (Scored)
          1.1.10 Ensure noexec option set on /var/tmp partition (Scored)
          1.1.11 Ensure separate partition exists for /var/log (Scored)
          1.1.12 Ensure separate partition exists for /var/log/audit (Scored)
          1.1.13 Ensure separate partition exists for /home (Scored)
          1.1.14 Ensure nodev option set on /home partition (Scored)
          1.1.15 Ensure nodev option set on /dev/shm partition (Scored)
          1.1.16 Ensure nosuid option set on /dev/shm partition (Scored)
          1.1.17 Ensure noexec option set on /dev/shm partition (Scored)
          1.1.18 Ensure nodev option set on removable media partitions (Not Scored)
          1.1.19 Ensure nosuid option set on removable media partitions (Not Scored)
          1.1.20 Ensure noexec option set on removable media partitions (Not Scored)
    debug:
      msg: "Ensure separate partitions exist and that partition options are set correctly"
    when: partitioning == 'yes'
    tags:
      - section01-1
      - section01-2
      - section01
      - level1
      - level2
      - section1.1.2
      - section1.1.3
      - section1.1.4
      - section1.1.5
      - section1.1.6
      - section1.1.7
      - section1.1.8
      - section1.1.9
      - section1.1.10
      - section1.1.11
      - section1.1.12
      - section1.1.13
      - section1.1.14
      - section1.1.15
      - section1.1.16
      - section1.1.17
      - section1.1.18
      - section1.1.19
      - section1.1.20

  - name: 1.1.21 Ensure sticky bit is set on all world-writable directories (Scored)
    shell: df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null
    failed_when: False
    changed_when: False
    check_mode: no
    register: sticky_bit_dirs
    tags:
      - section01-1
      - section01
      - level1
      - section1.1.21

  - name: 1.1.21 Ensure sticky bit is set on all world-writable directories (Scored)
    file:
      path: '{{ item }}'
      mode: a+t
    loop: '{{ sticky_bit_dirs.stdout_lines }}'
    tags:
      - section01-1
      - section01
      - level1
      - section1.1.21

  - name: 1.1.22 Disable Automounting (Scored)
    apk:
      name: autofs
      state: absent
    tags:
      - section01-1
      - section01-2
      - section01
      - level1
      - level2
      - section1.1.22

  - name: 1.1.23 Disable USB Storage (Scored)
    copy:
      src: files/etc/modprobe.d/cis_usb.conf
      dest: /etc/modprobe.d/cis_usb.conf
      owner: root
      group: root
      mode: '0644'
    tags:
      - section01-1
      - section01-2
      - section01
      - level1
      - level2
      - section1.1.23

  - name: 1.2.1 Ensure package manager repositories are configured (Not Scored)
    debug:
      msg: "Verify apk repositories are configured correctly"
    tags:
      - section01-1
      - section01
      - level1
      - section1.2.1

  - name: 1.2.1 Ensure package manager repositories are configured (Not Scored)
    replace:
      path: /etc/apk/repositories
      regexp: '^#(.*v.*community)'
      replace: '\1'
      backup: yes
    tags:
      - section01-1
      - section01
      - level1
      - section1.2.1

  - name: 1.2.2 Ensure GPG keys are configured (Not Scored)
    debug:
      msg: "Verify GPG keys are configured correctly"
    tags:
      - section01-1
      - section01
      - level1
      - section1.2.2

  - name: 1.3.1 Ensure AIDE is installed (Scored)
    debug:
      msg: "AIDE is not available in Alpine Linux. Placeholder until AIDE is."
    tags:
      - section01-1
      - section01
      - level1
      - section1.3.1
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"

  - name: 1.4.1 Ensure permissions on bootloader config are configured (Scored)
    file:
      path: '/boot/extlinux.conf'
      owner: root
      group: root
      mode: '0644'
    ignore_errors: True
    tags:
      - section01-1
      - section01
      - level1
      - section1.4.1
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"

  - name: 1.4.2 Ensure bootloader password is set (Scored)
    expect:
      command: 'grub2-setpassword'
      responses:
        (?i)password: '{{ grub_pass }}'
    notify: update grub
    tags:
      - section01-1
      - section01
      - level1
      - section1.4.2
    when:
      - ansible_os_family != "Alpine"
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"

  - name: 1.4.3 Ensure authentication required for single user mode (Scored)
    user:
      name: root
      password: "{{ root_pass | password_hash('sha512') }}"
    tags:
      - section01-1
      - section01
      - level1
      - section1.4.3
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"

  - name: 1.4.3 Ensure authentication required for single user mode (Scored)
    apk:
      name: 
        - linux-pam
        - shadow
        - bash
      state: present
    when:
      - ansible_os_family == "Alpine"
    tags:
      - section01-1
      - section01
      - level1
      - section1.4.3

  - name: 1.5.1 Ensure core dumps are restricted (Scored)
    copy:
      src: files/etc/security/limits.d/cis_limits.conf
      dest: /etc/security/limits.d/cis_limits.conf
      owner: root
      group: root
      mode: '0644'
    tags:
      - section01-1
      - section01
      - level1
      - section1.5.1

  - name: 1.5.2 Ensure XD/NX support is enabled (Not Scored)
    debug:
      msg: "Ensure XD/NX support is enabled by running: dmesg | grep 'NX.*active'"
    tags:
      - section01-1
      - section01
      - level1
      - section1.5.2

  - name: |
          1.5.1 Ensure core dumps are restricted (Scored)
          1.5.3 Ensure address space layout randomization (ASLR) is enabled (Scored)
    copy:
      src: files/etc/sysctl.d/60-cis_sysctl.conf
      dest: /etc/sysctl.d/60-cis_sysctl.conf
      owner: root
      group: root
      mode: '0644'
    tags:
      - section01-1
      - section01
      - level1
      - section1.5.1
      - section1.5.3

  - name: 1.5.4 Ensure prelink is disabled (Scored)
    command: "apk info prelink"
    args:
      warn: no
    failed_when: False
    changed_when: False
    register: prelink_installed
    tags:
      - section01-1
      - section01
      - level1
      - section1.5.4

  - name: 1.5.4 Ensure prelink is disabled (Scored)
    command: prelink -ua
    when: prelink_installed.stdout | length > 0
    tags:
      - section01-1
      - section01
      - level1
      - section1.5.4

  - name: 1.5.4 Ensure prelink is disabled (Scored)
    apk:
      name: prelink
      state: absent
    when: prelink_installed.stdout | length > 0
    tags:
      - section01-1
      - section01
      - level1
      - section1.5.4

  - name: 1.6.2.5 Ensure the MCS Translation Service (mcstrans) is not installed (Scored)
    apk:
      name: mcstrans
      state: absent
    tags:
      - section01-2
      - section01
      - level2
      - section1.6.2.5

  - name: 1.6.2.6 Ensure no unconfined daemons exist (Scored)
    debug:
      msg: "Ensure no unconfined daemons exist by running: ps -eZ | egrep \"initrc\" | egrep -vw \"tr|ps|egrep|bash|awk\" | tr ':' ' ' | awk '{ print $NF }'"
    tags:
      - section01-2
      - section01
      - level2
      - section1.6.2.6

  - name: 1.7.1.1 Ensure message of the day is configured properly (Scored)
    copy:
      dest: /etc/motd
      content: ''
      owner: root
      group: root
      mode: '0644'
    tags:
      - section01-1
      - section01
      - level1
      - section1.7.1.1
      - section1.7.1.4

  - name: |
          1.7.1.2 Ensure local login warning banner is configured properly (Scored)
          1.7.1.3 Ensure remote login warning banner is configured properly (Scored)
          1.7.1.5 Ensure permissions on /etc/issue are configured (Scored)
          1.7.1.6 Ensure permissions on /etc/issue.net are configured (Scored)
    copy:
      src: files/etc/issue
      dest: '/etc/{{ item }}'
      owner: root
      group: root
      mode: '0644'
    loop:
      - issue
      - issue.net
    tags:
      - section01-1
      - section01
      - level1
      - section1.7.1.2
      - section1.7.1.3
      - section1.7.1.5
      - section1.7.1.6

  - name: 1.7.2 Ensure GDM login banner is configured (Scored)
    copy:
      src: files/etc/dconf/profile/gdm
      dest: /etc/dconf/profile/gdm
      owner: root
      group: root
      mode: '0644'
    when: has_gui == "yes"
    notify: update dconf
    tags:
      - section01-1
      - section01
      - level1
      - section1.7.2

  - name: 1.7.2 Ensure GDM login banner is configured (Scored)
    file:
      path: /etc/dconf/db/gdm.d
      owner: root
      group: root
      mode: '0755'
      state: directory
    when: has_gui == "yes"
    tags:
      - section01-1
      - section01
      - level1
      - section1.7.2

  - name: 1.7.2 Ensure GDM login banner is configured (Scored)
    copy:
      src: files/etc/dconf/db/gdm.d/01-banner-message
      dest: /etc/dconf/db/gdm.d/01-banner-message
      owner: root
      group: root
      mode: '0644'
    when: has_gui == "yes"
    notify: update dconf
    tags:
      - section01-1
      - section01
      - level1
      - section1.7.2

  - name: 1.8 Ensure updates, patches, and additional security software are installed (Scored)
    apk:
      upgrade: yes
    tags:
      - section01-1
      - section01
      - level1
      - section1.8