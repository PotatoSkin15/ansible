---

  - name: 5.1.1 Ensure cron daemon is enabled (Scored)
    systemd:
      name: cron
      enabled: yes
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.1

  - name: 5.1.2 Ensure permissions on /etc/crontab are configured (Scored)
    file:
      path: /etc/crontab
      owner: root
      group: root
      mode: '0600'
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.2

  - name: |
          5.1.3 Ensure permissions on /etc/cron.hourly are configured (Scored)
          5.1.4 Ensure permissions on /etc/cron.daily are configured (Scored)
          5.1.5 Ensure permissions on /etc/cron.weekly are configured (Scored)
          5.1.6 Ensure permissions on /etc/cron.monthly are configured (Scored)
          5.1.7 Ensure permissions on /etc/cron.d are configured (Scored)
    file:
      path: '{{ item }}'
      state: directory
      owner: root
      group: root
      mode: '0700'
    loop:
      - /etc/cron.hourly
      - /etc/cron.daily
      - /etc/cron.weekly
      - /etc/cron.monthly
      - /etc/cron.d
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.3
      - section5.1.4
      - section5.1.5
      - section5.1.6
      - section5.1.7

  - name: 5.1.8 Ensure at/cron is restricted to authorized users (Scored)
    copy:
      dest: '{{ item }}'
      owner: root
      group: root
      mode: '0600'
      content: 'root'
    loop:
      - /etc/cron.allow
      - /etc/at.allow
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.8

  - name: 5.1.8 Ensure at/cron is restricted to authorized users (Scored)
    file:
      path: '{{ item }}'
      state: absent
    loop:
      - /etc/cron.deny
      - /etc/at.deny
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.1.8

  - name: |
          5.2.1 Ensure permissions on /etc/ssh/sshd_config are configured (Scored)
          5.2.2 Ensure SSH Protocol is set to 2 (Scored)
          5.2.4 Ensure SSH LogLevel is set to INFO (Scored)
          5.2.5 Ensure SSH X11 forwarding is disabled (Scored)
          5.2.6 Ensure SSH MaxAuthTries is set to 4 or less (Scored)
          5.2.7 Ensure SSH IgnoreRhosts is enabled (Scored)
          5.2.8 Ensure SSH HostbasedAuthentication is disabled (Scored)
          5.2.9 Ensure SSH root login is disabled (Scored)
          5.2.10 Ensure SSH PermitEmptyPasswords is disabled (Scored)
          5.2.11 Ensure SSH PermitUserEnvironment is disabled (Scored)
          5.2.12 Ensure only strong ciphers are used (Scored)
          5.2.13 Ensure only approved MAC algorithms are used (Scored)
          5.2.14 Ensure only strong Key Exchange algorithms are used (Scored)
          5.2.15 Ensure SSH Idle Timeout Interval is configured (Scored)
          5.2.16 Ensure SSH LoginGraceTime is set to one minute or less (Scored)
          5.2.18 Ensure SSH warning banner is configured (Scored)
          5.2.19 Ensure SSH PAM is enabled (Scored)
          5.2.20 Ensure SSH AllowTcpForwarding is disabled (Scored)
          5.2.22 Ensure SSH MaxSessions is limited (Scored)
    template:
      src: templates/etc/ssh/sshd_config.j2
      dest: /etc/ssh/sshd_config
      owner: root
      group: root
      mode: '0600'
    notify: restart sshd
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.2.1
      - section5.2.2
      - section5.2.4
      - section5.2.5
      - section5.2.6
      - section5.2.7
      - section5.2.8
      - section5.2.9
      - section5.2.10
      - section5.2.11
      - section5.2.12
      - section5.2.13
      - section5.2.14
      - section5.2.15
      - section5.2.16
      - section5.2.17
      - section5.2.18
      - section5.2.19
      - section5.2.20
      - section5.2.22

  - name: 5.2.2 Ensure permissions on SSH private host key files are configured (Scored)
    file:
      path: '/etc/ssh/{{ item }}'
      owner: root
      group: root
      mode: '0600'
    with_fileglob:
      - 'ssh_host_*_key'
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.2.2

  - name: 5.2.3 Ensure permissions on SSH public host key files are configured (Scored)
    file:
      path: '/etc/ssh/{{ item }}'
      owner: root
      group: root
      mode: '0644'
    with_fileglob:
      - 'ssh_host_*_key.pub'
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.2.3

  - name: 5.2.14 Ensure SSH access is limited (Scored)
    debug:
      msg: "Ensure SSH access is limited by setting up AllowUsers, AllowGroups, DenyUsers, and DenyGroups in /etc/ssh/sshd_config."
    tags:
      - section05-1
      - section05
      - level1
      - section5.2.14

  - name: 5.3.1 Ensure password creation requirements are configured (Scored)
    apt:
      name: libpam-pwquality
      state: present
    tags:
      - section05-1
      - section05
      - level1
      - section5.3.1

  - name: 5.3.1 Ensure password creation requirements are configured (Scored)
    template:
      src: templates/etc/security/pwquality.conf.j2
      dest: /etc/security/pwquality.conf
      owner: root
      group: root
      mode: '0644'
    tags:
      - section05-1
      - section05
      - level1
      - section5.3.1

  - name: 5.3.1 Ensure password creation requirements are configured (Scored)
    lineinfile:
      path: /etc/pam.d/common-password
      regexp: '^password(.*)requisite(.*)pam_pwquality.so'
      line: 'password\1requisite\2pam_pwquality.so retry={{ pwquality_retry }}'
      state: present
      backup: yes
      backrefs: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.3.1

  - name: 5.3.2 Ensure lockout for failed password attempts is configured (Scored)
    lineinfile:
      path: /etc/pam.d/login
      regexp: '^auth.*pam_tally2.so'
      line: "auth\trequired\t\tpam_tally2.so onerr=fail audit silent deny=5 unlock_time=900"
      state: present
      insertbefore: BOF
      backup: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.3.2

  - name: 5.3.3 Ensure password reuse is limited (Scored)
    blockinfile:
      path: /etc/pam.d/common-password
      block: |
        password	required			pam_pwhistory.so remember={{ pwhistory_remember }}
      state: present
      insertbefore: '.*the "Primary" block.*'
      backup: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.3.3

  - name: 5.3.4 Ensure password hashing algorithm is SHA-512 (Scored)
    lineinfile:
      path: /etc/pam.d/common-password
      regexp: '^password(.*)pam_unix.so'
      line: 'password\1pam_unix.so obscure use_authtok try_first_pass sha512'
      state: present
      backrefs: yes
      backup: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.3.4

  - name: |
          5.4.1.1 Ensure password expiration is 365 days or less (Scored)
          5.4.1.2 Ensure minimum days between password changes is 7 or more (Scored)
          5.4.1.3 Ensure password expiration warning days is 7 or more (Scored)
    lineinfile:
      path: /etc/login.defs
      regexp: '{{ item.regexp }}'
      line: '{{ item.line }}'
      state: present
    loop:
      - { regexp: '^(PASS_MAX_DAYS|#PASS_MAX_DAYS)', line: 'PASS_MAX_DAYS {{ pass_max_days }}' }
      - { regexp: '^(PASS_MIN_DAYS|#PASS_MIN_DAYS)', line: 'PASS_MIN_DAYS {{ pass_min_days }}' }
      - { regexp: '^(PASS_WARN_AGE|#PASS_WARN_AGE)', line: 'PASS_WARN_AGE {{ pass_warn_age }}' }
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.1.1
      - section5.4.1.2
      - section5.4.1.3

  - name: 5.4.1.4 Ensure inactive password lock is 30 days or less (Scored)
    lineinfile:
      path: /etc/default/useradd
      regexp: '^(INACTIVE|# INACTIVE|#INACTIVE)'
      line: 'INACTIVE={{ inactive_account_days }}'
      state: present
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.1.4

  - name: 5.4.1.5 Ensure all users last password change date is in the past (Scored)
    debug:
      msg: "Ensure all users last password change date is in the past."
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.1.5

  - name: 5.4.2 Ensure system accounts are non-login (Scored)
    shell: awk -F':' '($1!="root" && $1!="sync" && $1!="shutdown" &&$1!="halt" && $3<1000 && $7!="/usr/sbin/nologin" && $7!="/bin/false") {print $1}' /etc/passwd
    failed_when: False
    changed_when: False
    check_mode: no
    register: systemaccounts
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.2

  - name: 5.4.2 Ensure system accounts are non-login (Scored)
    user:
      name: '{{ item }}'
      shell: /usr/sbin/nologin
    loop: '{{ systemaccounts.stdout_lines }}'
    when:
      - systemaccounts.stdout_lines is defined
      - systemaccounts.stdout_lines != ""
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.2

  - name: 5.4.3 Ensure default group for the root account is GID 0 (Scored)
    user:
      name: root
      group: root
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.3

  - name: 5.4.4 Ensure default user umask is 027 or more restrictive (Scored)
    lineinfile:
      path: /etc/login.defs
      regexp: '^(UMASK|#UMASK)'
      line: "UMASK\t\t{{ user_umask }}"
      state: present
      backup: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.4

  - name: 5.4.4 Ensure default user umask is 027 or more restrictive (Scored)
    blockinfile:
      path: /etc/bash.bashrc
      block: |
             ## Set default user umask
             umask {{ user_umask }}
      state: present
      insertafter: EOF
      backup: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.4

  - name: 5.4.4 Ensure default user umask is 027 or more restrictive (Scored)
    template:
      src: templates/etc/profile.d/zz_cis.sh.j2
      dest: /etc/profile.d/zz_cis.sh
      owner: root
      group: root
      mode: '0644'
    tags:
      - section05-1
      - section05
      - level1
      - section5.4.4

  - name: 5.4.5 Ensure default user shell timeout is 900 seconds or less (Scored)
    debug:
      msg: "TMOUT interferes with long running Ansible tasks and sshd timeouts.  TMOUT provides no real benefit as a user can override the setting even when it is set to a read only value."
    tags:
      - section05-2
      - section05
      - level2
      - section5.4.5

  - name: 5.5 Ensure root login is restricted to system console (Not Scored)
    copy:
      dest: /etc/securetty
      owner: root
      group: root
      mode: '0644'
      content: ''
      backup: yes
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section05-1
      - section05
      - level1
      - section5.5

  - name: 5.6 Ensure access to the su command is restricted (Scored)
    lineinfile:
      path: /etc/pam.d/su
      regexp: '.*auth       required   pam_wheel.so$'
      line: 'auth       required   pam_wheel.so'
      state: present
      backup: yes
    tags:
      - section05-1
      - section05
      - level1
      - section5.6