---

  - name: 3.1.2 Ensure wireless interfaces are disabled (Not Scored)
    debug:
      msg: "Ensure wireless interfaces are disabled if not needed."
    tags:
      - section03-1
      - section03
      - level1
      - section3.1.2

  - name: |
          3.2.1 Ensure packet redirect sending is disabled (Scored)
          3.2.2 Ensure IP forwarding is disabled (Scored)
          3.3.1 Ensure source routed packets are not accepted (Scored)
          3.3.2 Ensure ICMP redirects are not accepted (Scored)
          3.3.3 Ensure secure ICMP redirects are not accepted (Scored)
          3.3.4 Ensure suspicious packets are logged (Scored)
          3.3.5 Ensure broadcast ICMP requests are ignored (Scored)
          3.3.6 Ensure bogus ICMP responses are ignored (Scored)
          3.3.7 Ensure Reverse Path Filtering is enabled (Scored)
          3.3.8 Ensure TCP SYN Cookies is enabled (Scored)
    copy:
      src: files/etc/sysctl.d/60-cis_sysctl_ipv4.conf
      dest: /etc/sysctl.d/60-cis_sysctl_ipv4.conf
      owner: root
      group: root
      mode: '0644'
    tags:
      - section03-1
      - section03
      - level1
      - section3.2.1
      - section3.2.2
      - section3.3.1
      - section3.3.2
      - section3.3.3
      - section3.3.4
      - section3.3.5
      - section3.3.6
      - section3.3.7
      - section3.3.8

  - name: |
          3.3.9 Ensure IPv6 router advertisements are not accepted (Not Scored)
          3.3 Ensure IPv6 redirects are not accepted (Not Scored)
    copy:
      src: files/etc/sysctl.d/60-cis_sysctl_ipv6.conf
      dest: /etc/sysctl.d/60-cis_sysctl_ipv6.conf
      owner: root
      group: root
      mode: '0644'
    when: use_ipv6 == "yes"
    tags:
      - section03-1
      - section03
      - level1
      - section3.3.9
      - section3.3

  - name: 3.3.1 Ensure IPv6 is disabled (Not Scored)
    lineinfile:
      path: /etc/default/grub
      regexp: '^GRUB_CMDLINE_LINUX="(.*)"'
      line: 'GRUB_CMDLINE_LINUX="\1 ipv6.disable=1"'
      backup: yes
      backrefs: yes
    when: 
      - use_ipv6 == "no"
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    notify: update grub
    tags:
      - section03-1
      - section03
      - level1
      - section3.3.1

  - name: |
          3.4.1 Ensure DCCP is disabled (Not Scored)
          3.4.2 Ensure SCTP is disabled (Not Scored)
          3.4.3 Ensure RDS is disabled (Not Scored)
          3.4.4 Ensure TIPC is disabled (Not Scored)
    copy:
      src: files/etc/modprobe.d/cis_np.conf
      dest: /etc/modprobe.d/cis_np.conf
      owner: root
      group: root
      mode: '0644'
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section03-1
      - section03
      - level1
      - section3.4.1
      - section3.4.2
      - section3.4.3
      - section3.4.4

  - name: 3.5.1.1 Ensure ufw is installed (Scored)
    apt:
      name: ufw
      state: present
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section03-1
      - section03
      - level1
      - section3.5.1.1

  - name: 3.5.1.3 Ensure ufw is enabled (Scored)
    systemd:
      name: ufw
      state: started
      enabled: yes
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section03-1
      - section03
      - level1
      - section3.5.1.3

  - name: 3.5.1.1 Ensure ufw is installed (Scored)
    ufw:
      rule: allow
      port: "22"
      proto: tcp
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section03-1
      - section03
      - level1
      - section3.5.1.1

  - name: |
          3.5.1.4 Ensure loopback traffic is configured (Scored)
          3.5.1.5 Ensure outbound and established connections are configured (Not Scored)
          3.5.1.6 Ensure firewall rules exist for all open ports (Scored)
    debug:
      msg: "Ensure ufw is configured correctly."
    tags:
      - section03-1
      - section03
      - level1
      - section3.5.1.4
      - section3.5.1.5
      - section3.5.1.6

  - name: 3.5.1.7 Ensure default deny firewall policy (Scored)
    ufw:
      state: enabled
      direction: incoming
      policy: reject
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section03-1
      - section03
      - level1
      - section3.5.1.7

  - name: |
          3 Ensure /etc/hosts.allow is configured (Scored)
          3 Ensure /etc/hosts.deny is configured (Scored)
    debug:
      msg: "Ensure /etc/hosts.allow and /etc/hosts.deny are configured."
    tags:
      - section03-1
      - section03
      - level1
      - section3

  - name: |
          3 Ensure permissions on /etc/hosts.allow are configured (Scored)
          3 Ensure permissions on /etc/hosts.deny are configured (Scored)
    file:
      path: '{{ item }}'
      owner: root
      group: root
      mode: '0644'
    loop:
      - /etc/hosts.allow
      - /etc/hosts.deny
    when:
      - ansible_virtualization_type != "docker"
      - ansible_virtualization_type != "kvm"
    tags:
      - section03-1
      - section03
      - level1
      - section3