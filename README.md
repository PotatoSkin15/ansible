# Playbooks

## Baseline

### Available OS Baselines

Note: "Server" refers to a bare-metal OS installation with no GUI. "Workstation" refers to a bare-metal OS installation with a GUI. "Container" refers to a container image.

|	      | Alpine3 | CentOS6 | CentOS7 | CentOS8 | Debian8 | Debian9 | Debian10 | SuSE15   | Ubuntu16 | Ubuntu18 | Ubuntu20 |
| :---:       | :---:   | :---:   | :---:   | :---:   | :---:   | :---:   | :---:    | :---:    | :---:    | :---:    | :---:    |
| Server      | **X**   | **X**   | **X**   | **X**   | **X**   | **X**   | **X**    | **X**    | **X**    | **X**    | **X**    |
| Workstation |
| Container   | **X**   | **X**   | **X**   | **X**   | **X**   | **X**   | **X**    | **X**    | **X**    | **X**    | **X**    |
